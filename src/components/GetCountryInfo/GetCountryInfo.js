import React, {Component} from 'react';
import './GetInfo.css'
import axios from 'axios';

class GetCountryInfo extends Component {

    state = {
        name: null,
        capital: '',
        population: '',
        region: '',
        flag: '',
        borders: [],
    };

    componentDidUpdate(prevProps) {
        if (this.props.code) {
            if (prevProps.code !== this.props.code)
            {
                axios.get('https://restcountries.eu/rest/v2/alpha/' + this.props.code).then(response => {
                    this.setState({
                        name: response.data.name,
                        capital: response.data.capital,
                        population: response.data.population,
                        region: response.data.region,
                        flag: response.data.flag,
                    });
                    return Promise.all(response.data.borders.map(alphaCode => {

                       return axios.get('https://restcountries.eu/rest/v2/alpha/' + alphaCode).then(country => {

                          return country.data.name
                       })
                    }))

                }).then(post => {
                    this.setState({borders: post})
                }).catch(error => {
                    console.log('Status is ' + error)
                })

            }
        }
    }

    render() {
        return (
            this.state.name ? <div className="information-block">
                <h1>Country: {this.state.name}</h1>
                <h3>Capital: {this.state.capital}</h3>
                <h3>Population: {this.state.population}</h3>
                <h3>Region: {this.state.region}</h3>
                <img src={this.state.flag} alt="" className="flag-img"/>
                <h5>Borders with: </h5>
                {this.state.borders.map((country, key) => {
                    return (
                        <li key={key}>{country}</li>
                    )
                })}
            </div> : <h2>Choose the country </h2>
        );
    }
}

export default GetCountryInfo;