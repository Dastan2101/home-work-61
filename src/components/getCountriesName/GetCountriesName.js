import React from 'react';
import './GetCountriesName.css';

const GetCountriesName = (props) => {
    return (
        <div className="countryName" onClick={props.selected}>
            <h4>{props.name}</h4>
        </div>
    );
};

export default GetCountriesName;