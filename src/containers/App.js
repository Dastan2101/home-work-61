import React, { Component } from 'react';
import axios from 'axios';
import GetCountriesName from "../components/getCountriesName/GetCountriesName";
import './App.css'
import GetCountryInfo from "../components/GetCountryInfo/GetCountryInfo";

class App extends Component {

  state = {
    countries: [],
      alpha3Code: null
  };

  componentDidMount() {
  axios.get('https://restcountries.eu/rest/v2/all?fields=name;alpha3Code').then(response => {
      this.setState({countries: response.data})
  })
  }


    alphaCodeSelectedHandler = code => {
    this.setState({alpha3Code: code})
  };

    render() {
    return (
      <div className="App">
        <div className="main-block">
        <div className="countries-name-block">
          {this.state.countries.map((country, key) => (
            <GetCountriesName key={key} name={country.name} selected={() => this.alphaCodeSelectedHandler(country.alpha3Code)} />
          ))}
        </div>
        <div className="Info-block">
        <GetCountryInfo code={this.state.alpha3Code} />
        </div>
      </div>
      </div>
    );
  }
}

export default App;
